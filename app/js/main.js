function onClickMenu() {
  var menuDisplay = document.getElementById('main-menu').style.display;

  switch (menuDisplay) {
    case 'none':
      document.getElementById('main-menu').style.display = 'flex';
      return;
    case 'flex':
      document.getElementById('main-menu').style.display = 'none';
      return;
    default:
      document.getElementById('main-menu').style.display = 'flex';
      return;
  }
}

function onResizeWindow() {
  if (window.outerWidth >= 960) {
    document.getElementById('main-menu').style.display = 'flex';
  } else {
    document.getElementById('main-menu').style.display = 'none';
  }
}

//https://www.w3schools.com/w3css/w3css_slideshow.asp

var slideIndex = 1;

function moveSlider(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  var i;
  var x = document.getElementsByClassName("slides");

  if (n > x.length) {
    slideIndex = 1
  } 

  if (n < 1) {
    slideIndex = x.length
  }

  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none"; 
  }

  x[slideIndex-1].style.display = "block"; 
}s