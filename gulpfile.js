'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');

gulp.task('build-sass', function() {
    return gulp.src('app/scss/estilos.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('app/css'));
});

gulp.task('watch', function() {
    gulp.watch('app/scss/*.scss', gulp.series('build-sass'));
});